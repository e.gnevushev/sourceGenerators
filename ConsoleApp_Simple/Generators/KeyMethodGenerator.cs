﻿using System.Text;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Text;

namespace Generators_Simple
{
    [Generator]
    public class KeyMethodGenerator : ISourceGenerator
    {
        public void Initialize(GeneratorInitializationContext context)
        {
        }

        public void Execute(GeneratorExecutionContext context)
        {
            context.AddSource("Dto.Generated.cs", SourceText.From(@"
namespace ConsoleApp_Simple
{
    public partial class Dto
    {
        public string GetKey() => Isin.ToString();

        public partial int GetMagicNumber() => 5;
    }
}", Encoding.UTF8));
        }
    }
}
