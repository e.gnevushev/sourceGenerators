﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Text;

namespace Generators_SaveOutput
{
    [Generator]
    public class Generator : ISourceGenerator
    {
        public void Initialize(GeneratorInitializationContext context)
        {
        }

        public void Execute(GeneratorExecutionContext context)
        {
            var file = context.AdditionalFiles.FirstOrDefault(f => Path.GetFileName(f.Path).Equals("Content.txt"));
            var content = file?.GetText()?.Lines.FirstOrDefault().ToString();

            if (string.IsNullOrWhiteSpace(content))
            {
                context.ReportDiagnostic(Diagnostic.Create(
                    "ERR_01",
                    "Source generator",
                    "Content is empty",
                    DiagnosticSeverity.Error,
                    DiagnosticSeverity.Error,
                    true,
                    0
                ));
                return;
            }

            context.AddSource("Dto.Generated.cs", SourceText.From($@"
using System;

namespace ConsoleApp_SaveOutput
{{
    public static class Example
    {{
        public static void Hello() => Console.WriteLine(""{content}"");
    }}
}}", Encoding.UTF8));
        }
    }
}
