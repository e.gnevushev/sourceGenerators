﻿using System;

namespace ConsoleApp_Simple
{
    class Program
    {
        static void Main()
        {
            var dto = new Dto {Isin = 13456};
            Console.WriteLine($"This key was generated: {dto.GetKey()}");
            Console.WriteLine($"This magic number was generated: {dto.GetMagicNumber()}");
        }
    }
}
