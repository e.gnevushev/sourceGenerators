#### Демонстрационный пример Roslyn Source Generator

##### Дополнительные материалы
- https://github.com/dotnet/roslyn/blob/main/docs/features/source-generators.md
- https://github.com/dotnet/roslyn/blob/main/docs/features/source-generators.cookbook.md
- https://gist.github.com/TessenR/ab40df2d6e971a8d6e5c6c6295d85d11
- https://notanaverageman.github.io/2020/12/07/cs-source-generators-cheatsheet.html

##### Готовые генераторы:
- генерация классов по json модели
    https://github.com/hermanussen/JsonByExampleGenerator

- сериализация объекта в json без рефлексии
    https://github.com/trampster/JsonSrcGen

- генерируемые обертки на PInvoke (win32)
    https://github.com/microsoft/CsWin32

- подборка интересных генераторов
    https://github.com/amis92/csharp-source-generators
